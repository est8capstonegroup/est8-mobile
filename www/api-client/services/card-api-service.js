angular.module('starter.controllers')

.factory('ApiClient', ['$resource',
	function($resource){
		// Set the URL of the site that will serve our API.
		// ---------------------------------------------------------------------------
		// We're using a local proxy to circumvent the CORS problem:
		// Read at http://blog.ionic.io/handling-cors-issues-in-ionic/
		// As for the proxy, refer to the ionic.project included in the zip root
		// ---------------------------------------------------------------------------
		// var apiBaseUrl = 'http://audacitus.com/tinderonic-json/';	//Demo Api JSON URL
		//var apiBaseUrl = 'http://localhost:8100/api/';	//Needs a trailing slash
		var apiBaseUrl = 'https://est8api-iokadigbo.rhcloud.com/api';
		var apiFilterUrl = 'https://est8api-iokadigbo.rhcloud.com/filter';
		var apiEmailUrl = 'https://est8api-iokadigbo.rhcloud.com/emailmsg';
		// ---------------------------------------------------------------------------

		// Resource-to-endpoints. Change this to your preferrence. Refer to www/json for the expected structure
		return {
			//CardStack: $resource(apiBaseUrl+'cardlist.php'),
			CardStack: $resource(apiBaseUrl),
			//call the datafilterservice getFilterData function, this is only called if the getfilterdata is not empty handled in AjaxCardsCtrl Controller
			FilteredCardStack: $resource(apiFilterUrl,{},{post: {method: 'POST',isArray: true, headers:{'Content-Type':'application/json; charset=UTF-8'}}}),
      ProfileDetails: $resource(apiBaseUrl+'/:profileID',{profileID: '@profileID'}),
			EmailSender: $resource(apiEmailUrl,{},{post: {method: 'POST',isArray: true, headers:{'Content-Type':'application/json; charset=UTF-8'}}}),
		};
	}]);
