angular.module('services', [])

.service('UserService', function() {
  if(window.localStorage.starter_facebook_user){
    var _user = JSON.parse(window.localStorage.starter_facebook_user || '{}');
  }

//for the purpose of this example I will store user data on ionic local storage but you should save it on a database

  var setUser = function(user_data) {
    _user = user_data;
  //  window.localStorage.starter_facebook_user = JSON.stringify(user_data);
  window.localStorage.starter_facebook_user = JSON.stringify(_user);
  };

  var getUser = function(){
//    return JSON.parse(window.localStorage.starter_facebook_user || '{}');
  return _user;
  };
    var deleteUser = function(){
        window.localStorage.removeItem("starter_facebook_user");
        
    };

  return {
    getUser: getUser,
    setUser: setUser,
    isLoggedIn: function () {
         return _user ? true : false;
      },
    deleteUser: deleteUser
  };
})
.service('UserSwipeRightService', function(){
    //store users likes into and object, so we can use this object to fetch the likes from the right tab controller. would store in local storage and on db
    var likes = new Array();
/*    Storage.prototype.setArray = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj));
  }
  Storage.prototype.getArray = function(key) {
    return JSON.parse(this.getItem(key));
  }*/
    var setRightSwipe = function(listing){
        likes.push(listing);
        window.localStorage.user_Right_Swipes = JSON.stringify(likes);
      //  window.localstorage.setArray("userLikes",likes);
    };
    var getRightSwipe = function(){
         return JSON.parse(window.localStorage.user_Right_Swipes || '{}');
    };
    return{
        setRightSwipe: setRightSwipe,
        getRightSwipe: getRightSwipe
    };
})
//grab user search criteria here
.service('DataFilterService',function(){
   var data = {};
    // function to share search data object to make call to server
  var setFilterData = function(query_data){
    data = query_data;
  };
  var getFilterData = function(){
    return data;
  };
  return {
    setFilterData: setFilterData,
    getFilterData: getFilterData
  };
});
