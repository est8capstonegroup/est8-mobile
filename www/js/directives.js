"use strict";


angular.module("directive.general", [])

.directive('disableSideMenu', [
    '$ionicSideMenuDelegate',
    function ($ionicSideMenuDelegate) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.on('touch', function () {
                    scope.$apply(function () {
                        $ionicSideMenuDelegate.canDragContent(false);
                    });
                });

                element.on('release', function () {
                    scope.$apply(function () {
                        $ionicSideMenuDelegate.canDragContent(true);
                    });
                });
            }
        };
    }
]);


.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
