angular.module('starter.controllers', ['ngSanitize'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

})


.controller('WalkthroughCtrl', function($scope, $state, $ionicSlideBoxDelegate) {

  $scope.slideIndex = 0;
  $scope.slideCount = 3;  //$ionicSlideBoxDelegate.slidesCount() currently returns underfined. Looking into this
  $scope.slideCount = $scope.slideCount - 1;

  $scope.leftButton = {
    text: 'Previous',
    show: false,
    tapFxn: function(e){
      // console.log('Tap Function Activated');
      $scope.previous();
    }
  }

  $scope.rightButton = {
    text: 'Next',
    show: true,
    tapFxn: function(e){
      // console.log('Tap Function Activated');
      $scope.next();
    }
  }

  // Called to navigate to the main app
  $scope.startApp = function() {
    $state.go('premiumappOne.cardSwipe');
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
    console.log($scope.slideIndex);
    // In between first and last slide
    if($scope.slideIndex > 0)
    {
      $scope.leftButton.show = true;
      $scope.rightButton.show = true;
      $scope.rightButton.text = 'Next';
      $scope.rightButton.tapFxn = function(){
        $scope.next();
      }
    }

    if($scope.slideIndex < 1)
    {
      $scope.leftButton.show = false;
      $scope.rightButton.show = true;
    }

    if($scope.slideIndex == $scope.slideCount)
    {
      console.log('last slide');
      $scope.rightButton.text = 'Go to App';

      $scope.rightButton.tapFxn = function(){
        console.log('function to launch app success!!!');
        $scope.startApp();
      }
    }

  };
})


.controller('TinderProfileCtrl', function($scope, ApiClient, LoaderService, $stateParams,$cordovaGeolocation){

  // Run the LoaderService ("Loading....")
  LoaderService.show();
  $scope.profileID = $stateParams.profileID;
  console.log('profileID is = '+$scope.profileID);
  $scope.ajaxRequest = ApiClient.ProfileDetails.query({profileID : $scope.profileID });
  $scope.ajaxRequest.$promise.then(function(){
    // If API Call is successful
    LoaderService.hide();
   // $scope.profileDetails = $scope.ajaxRequest;
      //trying to get single profile object. ***workaround***
     var listingProfile = $scope.ajaxRequest;
      $scope.profileDetails = listingProfile[0];

  },
    //If API call fails
  function(){
              var ErrorMessage = 'Api Call not successful. Check your apiBaseUrl (www/api-client/services/card-api-service.js) or ensure your proxy is well configured';
              console.error(ErrorMessage);
              LoaderService.errorLoading('Something went wrong. Please try again later');

              //Set Deck to empty to enable the user reload the cards
              $scope.deckIsEmpty = true;
          }

  )
$scope.message = {};
  $scope.sendMessage = function(msg){
    $scope.message = angular.copy(msg);
    $scope.message.realtorId = $scope.profileDetails.createdBy;
    $scope.message.subject = "New Interest in " + $scope.profileDetails.address + " From " + $scope.message.from;
    console.log($scope.message);
    $scope.returnMessage = ApiClient.EmailSender.post($scope.message);
    console.log($scope.returnMessage);
  }

})

.controller('AjaxCardsCtrl', function($scope, ApiClient, LoaderService, UserSwipeRightService,DataFilterService, $stateParams) {
  console.log('CARDS CTRL Initiated');
  function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

  $scope.cardsControl = {};
  //we'll need a variable to tell us if the deck is empty or full. Default is false
  // $scope.deckIsEmpty = false;

  //Reloading the cards.
  $scope.reload = function() {

      // Run the LoaderService ("Loading....")
      LoaderService.show()

      //We're making an ajax request to an endpoint giving us the json.
      //Refer to www/api-client/services/card-api-servie.js
      //if there is no filter parameter, fetch all from get request, else perform post request with filtered params
      if(!isEmpty(DataFilterService.getFilterData())){
        console.log("IT IS NOT EMPTY!!!! HOUSTON WE HAVE A PROBLEM");
          $scope.ajaxRequest = ApiClient.FilteredCardStack.post(DataFilterService.getFilterData());
      }
      else{
        console.log("We are skipping search, Watson")
        $scope.ajaxRequest = ApiClient.CardStack.query();
    }
      // If promise is successful, attach json to cardTypes variable
        $scope.ajaxRequest.$promise.then(function(){
              // Function for successful api call
              // End the LoaderService
              LoaderService.hide();
              cardTypes = $scope.ajaxRequest;
              console.log(cardTypes);

              $scope.cards = Array.prototype.slice.call(cardTypes, 0);

              //we'll need to have a counter for the cards deck
              $scope.cardCounter = $scope.cards.length;

              //we'll need a variable to tell us if the deck is empty or full. since we're reloading, default is false
              // $scope.deckIsEmpty = false;

              //we'll clone our $scope.cards for our own custom functions (like counting and exposing card data)
              $scope.cardDataArray = $scope.cards.slice(0);

              //If a card is swyped, its details will always be here. if we are resetting/updating the stack,
              // this variable will be reset. refer to the $scope.exposeSwypedCard function
              $scope.swypedCard = null;

              // debug data
              console.log('cards in deck: '+$scope.cards.length);

          },
          // Function for error handling
          function(){
              var ErrorMessage = 'Api Call not successful. Check your apiBaseUrl (www/api-client/services/card-api-service.js) or ensure your proxy is well configured';
              console.error(ErrorMessage);
              LoaderService.errorLoading('Something went wrong. Please try again later');

              //Set Deck to empty to enable the user reload the cards
              $scope.deckIsEmpty = true;
          }
        )

  }

  $scope.exposeSwypedCard = function() {
    //since a card has been removed from deck, reduce counter by 1
    //we're doing this to balance the 0-notation of arrays vs the array.lenght
    $scope.cardCounter -= 1;

    //if deck is empty set variable to true
    if ($scope.cardCounter === 0){
      $scope.deckIsEmpty = true;
      console.log('deck is empty!');
    }
   //function to resize cards
   $scope.resizeCard = function(imageUrl){
     var imgUrl1 = imageUrl.slice(0,45);
     var imgUrl2 = imageUrl.slice(45);
     var imgSize = "w_384,h_412,c_fit";
    var editedUrl = imgUrl1+imgSize+'/'+imgUrl2;
    console.log(editedUrl);
    return editedUrl;
   };

    //we'll use the cardCounter as the index in our cloned array for that card's data
    $scope.swypedCard = $scope.cardDataArray[$scope.cardCounter];


    //output to console. use to your preference (return it or use the $scope.swypedCard variable itself)
    console.log($scope.swypedCard);
      //Add to the swipedcard service
    UserSwipeRightService.setRightSwipe($scope.swypedCard);
  }

  //Takes out the swiped card data from the original array $scope.cards
  $scope.cardDestroyed = function(index) {
    $scope.cards.splice(index, 1);
  };

  $scope.addCard = function() {
    var newCard = cardTypes[Math.floor(Math.random() * cardTypes.length)];
    newCard.id = Math.random();
    $scope.cards.push(angular.extend({}, newCard));
  };

  // On tapping the accept function - triggers the $scope.cardSwipedRight() function
  $scope.yesClick = function() {
    $scope.cardsControl.swipeRight();
  };

  // On tapping the reject function triggers the $scope.cardSwipedLeft () function
  $scope.noClick = function() {
    $scope.cardsControl.swipeLeft();
  };

  //Callback Function on swiping to the left
  $scope.cardSwipedLeft = function(index) {
    console.log('LEFT SWIPE');
    $scope.exposeSwypedCard();
  };

  //Callback Function on swiping to the right
  $scope.cardSwipedRight = function(index) {
    console.log('RIGHT SWIPE');
    $scope.exposeSwypedCard();
  };

  $scope.reload()
})

.controller('LocalJSONCtrl', function($scope, ApiClient, LoaderService) {
  console.log('Local JSON CARDS CTRL Initiated. Pulling data from www/json');

  $scope.cardsControl = {};
  //we'll need a variable to tell us if the deck is empty or full. Default is false
  // $scope.deckIsEmpty = false;

  //Reloading the cards.
  $scope.reload = function() {

      // Run the LoaderService ("Loading....")
      LoaderService.show()

      //We're making an ajax request to an endpoint giving us the json.
      //Refer to www/api-client/services/card-api-servie.js
      $scope.ajaxRequest = ApiClient.LocalJSONCards.query();

      // If promise is successful, attach json to cardTypes variable
        $scope.ajaxRequest.$promise.then(function(){
              // Function for successful api call
              // End the LoaderService
              LoaderService.hide();
              cardTypes = $scope.ajaxRequest;
              console.log(cardTypes);

              $scope.cards = Array.prototype.slice.call(cardTypes, 0);

              //we'll need to have a counter for the cards deck
              $scope.cardCounter = $scope.cards.length;

              //we'll need a variable to tell us if the deck is empty or full. since we're reloading, default is false
              // $scope.deckIsEmpty = false;

              //we'll clone our $scope.cards for our own custom functions (like counting and exposing card data)
              $scope.cardDataArray = $scope.cards.slice(0);

              //If a card is swyped, its details will always be here. if we are resetting/updating the stack,
              // this variable will be reset. refer to the $scope.exposeSwypedCard function
              $scope.swypedCard = null;

              // debug data
              console.log('cards in deck: '+$scope.cards.length);

          },
          // Function for error handling
          function(){
              // var ErrorMessage = 'Api Call not successful. Check your apiBaseUrl (www/api-client/services/card-api-service.js) or ensure your proxy is well configured';
              // console.error(ErrorMessage);
              LoaderService.errorLoading('Something went wrong. Please try again later');

              //Set Deck to empty to enable the user reload the cards
              $scope.deckIsEmpty = true;
          }
        )

  }

  $scope.exposeSwypedCard = function() {
    //since a card has been removed from deck, reduce counter by 1
    //we're doing this to balance the 0-notation of arrays vs the array.lenght
    $scope.cardCounter -= 1;

    //if deck is empty set variable to true
    if ($scope.cardCounter === 0){
      $scope.deckIsEmpty = true;
      console.log('deck is empty!');
    }


    //we'll use the cardCounter as the index in our cloned array for that card's data
    $scope.swypedCard = $scope.cardDataArray[$scope.cardCounter];


    //output to console. use to your preference (return it or use the $scope.swypedCard variable itself)
    console.log($scope.swypedCard);
  }

  //Takes out the swiped card data from the original array $scope.cards
  $scope.cardDestroyed = function(index) {
    $scope.cards.splice(index, 1);
  };

  $scope.addCard = function() {
    var newCard = cardTypes[Math.floor(Math.random() * cardTypes.length)];
    newCard.id = Math.random();
    $scope.cards.push(angular.extend({}, newCard));
  };

  // On tapping the accept function - triggers the $scope.cardSwipedRight() function
  $scope.yesClick = function() {
    $scope.cardsControl.swipeRight();
  };

  // On tapping the reject function triggers the $scope.cardSwipedLeft () function
  $scope.noClick = function() {
    $scope.cardsControl.swipeLeft();
  };

  //Callback Function on swiping to the left
  $scope.cardSwipedLeft = function(index) {
    console.log('LEFT SWIPE');
    $scope.exposeSwypedCard();
  };

  //Callback Function on swiping to the right
  $scope.cardSwipedRight = function(index) {
    console.log('RIGHT SWIPE');
    console.log(index);
    $scope.exposeSwypedCard();
  };

  $scope.reload()
})
/*************** CONTROLLERS FROM INITIAL APP, FIND A WAY TO SEPARATE CONTROLLERS ***********/
.controller('WelcomeCtrl', function($scope, $state, $q, UserService, $ionicLoading,$ionicHistory) {

  //This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {
      //for the purpose of this example I will store user data on local storage
      UserService.setUser({
        authResponse: authResponse,
				userID: profileInfo.id,
				name: profileInfo.name,
				email: profileInfo.email,
        picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
      });

      $ionicLoading.hide();
      //disable history
      $ionicHistory.nextViewOptions({
        disableBack: true
    });

      $state.go('app.home');
  //  $state.go('app.location');

    }, function(fail){
      //fail get profile info
      console.log('profile info fail', fail);
    });
  };


  //This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  //this method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
        //saving the userID and name in localStorage
        //localStorage.setItem("uniqueId", response.id);
        //localStorage.setItem("loginName", response.name);



				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookSignIn = function() {

    facebookConnectPlugin.getLoginStatus(function(success){

      //window.localStorage.setItem( ‘applozicID’, success.authResponse.userID);

     if(success.status === 'connected'){
       //alert("i passed");
        // the user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);

				//check if we have our user saved
				var user = UserService.getUser('facebook');


				if(!user.userID)
				{
					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

						//for the purpose of this example I will store user data on local storage
						UserService.setUser({
							authResponse: success.authResponse,
							userID: profileInfo.id,
							name: profileInfo.name,
							email: profileInfo.email,
							picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
						});

						$state.go('app.home');
        //  $state.go('app.location');

					}, function(fail){
						//fail get profile info
						console.log('profile info fail', fail);
					});
				}else{

					$state.go('app.home');
      //  $state.go('app.location');
				}

     } else {
        //if (success.status === 'not_authorized') the user is logged in to Facebook, but has not authenticated your app
        //else The person is not logged into Facebook, so we're not sure if they are logged into this app or not.
        console.log('getLoginStatus', success.status);

			  $ionicLoading.show({

          template: 'Logging in...'
        });

        //ask the permissions you need. You can learn more about FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };
})

/*** Map Controller
.controller('MapCtrl', function($scope, $state, $cordovaGeolocation) {
  var options = {timeout: 10000, enableHighAccuracy: true};
 $scope.mapOptions = {};
  $cordovaGeolocation.getCurrentPosition(options).then(function(position){

    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    $scope.mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.map = new google.maps.Map(document.getElementById("map"), $scope.mapOptions);

  }, function(error){
    console.log("Could not get location");
  });
//  $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
    //Wait until the map is loaded
google.maps.event.addListenerOnce($scope.map, 'idle', function(){

  var marker = new google.maps.Marker({
      map: $scope.map,
      animation: google.maps.Animation.DROP,
      position: $scope.mapOptions.center
  });

});
})
 Enf of map Controller ***/

.controller('AppCtrl', function($scope){

})
.controller('LikesCtrl', function($scope, $state, UserSwipeRightService){
    //$scope.likes = [];
  /** URGENT!!!!!!!
  Adding to the wrong likes, add to scope.likes,
 this means using the same controller for both right swipe and side menu.
 or check on windows local storage why not live updating
  **/
    var checkIfNull = function(){
        if(UserSwipeRightService.getRightSwipe() == null){
            $scope.likes = null;
        }
        else{
            $scope.likes = UserSwipeRightService.getRightSwipe();
            console.log($scope.likes);
        }
    };
    checkIfNull();
  /*  $scope.clicked = function(){
        console.log($scope.likes);
    } */
})

.controller('HomeCtrl', function($scope, UserService, $ionicActionSheet, $state, $ionicLoading,$ionicHistory){

	$scope.user = UserService.getUser();

  $scope.showLogOutMenu = function() {
    console.log("Logging out suckers!!!");
  //  $ionicLoading.show({
  //    template: 'Logging out...'
  //  });
      UserService.deleteUser();
       $state.go('welcome');

    //facebook logout
    facebookConnectPlugin.logout(function(){
  //    $ionicLoading.hide();
       // UserService.deleteUser();
        console.log("we are in the facebook connect plugin...");
      $state.go('welcome');
    },
    function(fail){
      $ionicLoading.hide();
    });
  };
  //disable history
  $ionicHistory.nextViewOptions({
   disableBack: true
 });


})
// FILTER PAGE CONTROLLER
.controller('FilterCtrl', function($scope,$state,DataFilterService){
  $scope.searchParams = {};
    $scope.submitFilterParams = function(search){
    //  var testData = {"location": "Ontario", "minPrice": "100", "maxPrice": "1000000", "noOfBeds": "3", "type": "House", "noOfBaths": "3"};
      $scope.searchParams = angular.copy(search);
      DataFilterService.setFilterData($scope.searchParams);
      console.log($scope.searchParams);
    //  console.log(DataFilterService.getFilterData());
      $state.go('premiumappOne.cardSwipe');
    }
})

//search controller
.controller('SearchCtrl', function($scope,$state,DataFilterService){
    $scope.locationEnter = function(search){
      $scope.params = angular.copy(search);
      $scope.params.location = $scope.params.location.formatted_address;
      console.log($scope.params.location);
      DataFilterService.setFilterData($scope.params);
      console.log(DataFilterService.getFilterData());
      $state.go('premiumappOne.cardSwipe');
    }
})

//directive to ensure ng-enter works
.directive('ng-Enter', function(){
  return function(scope,element,attrs){
    element.bind("keydown keypress", function (event){
      if(event.which === 13){
        scope.$apply(function(){
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
})

//social sharing controller (affects profile.html)
.controller('Social', ['$scope', function($scope){
  $scope.share = function(socialMedia, msg, img, link){
    if(socialMedia == 'f')
    window.plugins.socialsharing
    .shareViaFacebook(msg, img, link);
    else if(socialMedia == 't')
    window.plugins.socialsharing
    .shareViaTwitter(msg, img, link);
    else if(socialMedia == 'w')
    window.plugins.socialsharing
    .shareViaWhatsApp(msg, img, link);
    else {};
  }
}])
